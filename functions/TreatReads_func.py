
from functions import *
# from AddRead import AddRead

def TreatReads(SAM, nReads, OUTPUT, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY):

	validReads = 0

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}


	i=0
	for q in quals_str:
		quals[quals_str[i]] = i
		i += 1


	# pileup = ParseBED(BED)

	with open(OUTPUT+"/.pileup_ini", 'rb') as handle:
		pileup = msgpack.unpack(handle, encoding="utf-8")
		pileup = SortPileup(pileup)


	currentLine = 1.0
	lastProgress = 0.0
	# totalLines = GetTotalLines(SAM)
	totalLines = nReads


	ALL_UMIS = {}

	
	for line in open(SAM):

		lastProgress = PrintProgress(currentLine, totalLines, lastProgress)

		currentLine += 1


		line = line.split('\t')
		
		try:
			umi = line[0].split('_')[-1]
		except:
			continue


		try:
			flag = int(line[1])
		except:
			continue
		
		# get strand : 0 = forward | 1 = reverse
		strand = int("{0:b}".format(int(flag))[-5])
		firstInPair = bool(int("{0:b}".format(int(flag))[-7]))
		chrom = line[2]
		pos = line[3]
		mapq = int(line[4])
		cigar = line[5]
		seq = line[9]
		qual = line[10]

		# build new_umi as umi + "-" + position of forward reads 
		# this is the default behavior as it allows to treat the 
		# case when the length of the umi tag is relatively short
		# => not enough combinations possible => different DNA 
		# fragments end up with the same umi tag. By adding the
		# alignment position in the umi tag, the tag becomes the 
		# umi sequence + the alignment position. if the read is 
		# forward => the start position is used , if the read is 
		# reverse => the mate strat position is used instead.
 
		matePos = line[7]
		# umiPos = int(str(pos)[-4:]) if strand == 0 else int(str(matePos)[-4:])
		

		# ini_umi = umi+"-"+str(umiPos)
		# found = False

		# for i in range(umiPos - 25, umiPos + 25):
		# 	test_umi = umi+"-"+str(i)
		# 	if test_umi in ALL_UMIS:
		# 		umi = test_umi
		# 		ALL_UMIS[test_umi] += 1
		# 		found = True
		# 		break
		
		# if not found:
		# 	umi = umi+"-"+str(umiPos)
		# 	ALL_UMIS[umi] = 1
		# 	print("added: "+umi)


		# if umi != ini_umi:
		# 	print(ini_umi)
		# 	print(umi)
		# 	print('\n')

		
		# umi = umi+"-"+str(umiPos)


		if ReadIsValid(flag, chrom, pos, umi, cigar, mapq, MIN_MAPPING_QUALITY, qual, quals, MIN_READ_QUALITY):
			
			AddRead(pileup, umi, strand, chrom, int(pos), cigar, seq, qual, quals, MIN_BASE_QUALITY)
			validReads += 1



	# for x in ALL_UMIS:
	# 	print(x+' : '+str(ALL_UMIS[x]))


	return [pileup, validReads]
