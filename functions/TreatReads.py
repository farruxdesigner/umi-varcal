import os
import sys
import time
import math
import msgpack
from pyfasta import Fasta


# import local modules
from functions import *
# from TreatReads_func import TreatReads

# launch the function that analyzes a set of reads
# this function will create a pileup from these reads
value = TreatReads(sys.argv[1], int(sys.argv[2]), sys.argv[3], float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]))
pileup = value[0]
validReads = str(value[1])


# dump pileup in msgpack object
with open(sys.argv[3]+"/"+sys.argv[1].replace(".sam", ".pileup").split("/")[-1], 'wb') as handle:
	msgpack.pack(pileup, handle)

# dump n valid reads in a file
with open(sys.argv[3]+"/."+sys.argv[1].replace(".sam", ".valid").split("/")[-1], 'w') as handle:
	handle.write(validReads)

