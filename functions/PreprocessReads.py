
# THIS FUNCTION IS CALLED TO SPLIT THE INITIAL SAM FILE INTO N SAM SUBFILES, N CORRESPONDING
# TO THE NUMBER OF CORES USED TO EXECUTE THE PROGRAM. THEREFORE, EACH CORE WILL ANALYZE A 
# SUBFILE, ALLOWING THEREFORE ALL THE SUBFILES TO BE ANALYZED AT THE SAME TIME BY ALL THE 
# PRECISED CORES TO BE USED.
#
# INPUT : 
# 		  -FILENAME                     : (STR)    THE PATH OF THE INITIAL SAM FILE
# 		  -TOTALLINES                   : (FLOAT)  TOTAL LINES IN THE FILE
# 		  -CORES                        : (INT)    NUMBER OF CORES TO BE USED
#
# VALUE : -SUBFILES                     : (LIST)   A LIST CONTAINING THE NAMES OF THE CREATED SUBFILES
#		
#

import os
import sys

from func import PrintTime, PrintProgress


def PreprocessReads(fileName, cores):

	# if only one core is to be used, no need to split the file
	# return the initial file
	if cores == 1:
		return [fileName]




	# print("\n")
	# PrintTime('console', "\tPreprocessing Reads...")


	# create an empty list to contain the created sub files names
	subFiles = []

	i = 0
	while i < cores:
		i_str = "0"+str(i) if i < 10 else str(i)
		subFiles.append(fileName.replace('.sam', '_'+i_str+'.sam'))
		i += 1
 

	command = "split -n l/"+str(cores)+" -d -a 2 --additional-suffix .sam "+fileName+" "+fileName.replace('.sam', '_')
	os.system(command)

	
	# print("\n")
	# PrintTime('console', "\tDone")

	# return the subfiles list 
	return subFiles


