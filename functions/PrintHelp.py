
### import functions from the script in the same dir
from func import *


def PrintHelp(tool, VERSION, LAST_UPDATE):

	tools = ["general", "extract", "call"]

	messages= []
	message = """
GENERAL DESCRIPTION
===============================================

Software for analyzing BAM/SAM files produced by an Illumina MiSeq sequencer using UMI (Unique Molecular Identifiers).
The software starts by building a pileup based on the regions specified in the BED file. This software integrates an  
extraction tool that can be used to extract UMI tags from the start of the reads. The software has also a calling tool
for calling variants and that outputs the results under VCF format.
Although SAM files generated by an Illumina MiSeq sequencer were used to develop this tool, it should work on BAM/SAM 
files generated by any other sequencer as long as they respect the Sequence Alignment/Map Format Specification.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Author: Pierre-Julien VIAILLY, Centre Henri Becquerel, Rouen, France
:Version: """+VERSION+"""
:Last updated: """+LAST_UPDATE+"""
:Tags: Genomics, UMI, Illumina, Miseq, Variant Calling


There are 2 tools available in this software:

	-extract
	-call


To get help on a specific tool, type:

	python3 umi-varcal.py <tool>
	or 
	python3 umi-varcal.py <tool> --help
	or 
	python3 umi-varcal.py <tool> -h

To use a specific tool, type:

	python3 umi-varcal.py <tool> [tool arguments]\n\n"""

	messages.append(message)





	message = """
TOOL DESCRIPTION : extract
===============================================

The extraction tool allows to extract the UMI sequence from the SAM/BAM sequence 
and add it to the end of the read ID, preceeded by an '_'. 
The UMI is always extracted from the start of the sequence.
Extracting the UMI from within the sequence is not handled by this tool.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Author: Pierre-Julien VIAILLY, Centre Henri Becquerel, Rouen, France
:Version: """+VERSION+"""
:Last updated: """+LAST_UPDATE+"""
:Tags: Genomics, UMI, Illumina, Miseq, Variant Calling, Alignment


USAGE
===============================================

python3 umi-varcal.py extract [required_arguments] [optional_arguments]


DEPENDENCIES
===============================================

-bwa
-Python module: pysam


REQUIRED ARGUMENTS
===============================================

-f, --fasta=FILE (case sensitive)
	The reference genome in FASTA format with its index present
	in the same directory.
	It is recommended that you enter the FASTA file full path
	For example : /home/my_dir/my_subdir/hg19.fasta

-i, --input=FILE (case sensitive)
	The input file SAM/BAM generated by the sequencer.
	It is recommended that you enter the input file full path
	For example : /home/my_dir/my_subdir/ind1.sam

-l, --umi_length=INT 
	umi_length : the length of the UMI sequence to extract
	The value must be a valid integer
	If not precised, the default value (umi_length = 12) will be used


OPTIONAL ARGUMENTS
===============================================

-t, --bwa_threads=INT 
	The number of cores to be used for the bwa alignment. Using more
	cores will significantly reduce the alignment time.
	The value of this parameter defaults to 1


EXAMPLE
===============================================

python3 umi-varcal.py extract -i /home/.../test.sam -f /home/.../hg19.fa -l 12 -t 5


OUTPUT
===============================================

The tool will automatically create a directory in the current directory 
called EXTRACTED. It will contain a folder named after the sample input.
In this directory, you can find the extracted R1 FASTQ, the extracted R2
FASTQ, the extracted BAM (with its index) and the extracted SAM file.\n\n"""

	messages.append(message)






	message = """
TOOL DESCRIPTION : call
===============================================

A tool for calling variants in a SAM/BAM file from which the UMI tags were extracted.
The calling tool starts by building a pileup based on the regions specified in the BED file. Given that the UMI tags
had already been extracted correctly, this tool will call variants and output the results under VCF format.
Although BAM/SAM files generated by an Illumina MiSeq sequencer were used to develop this tool, it should work on 
BAM/SAM files generated by any other sequencer as long as they respect the Sequence Alignment/Map Format Specification.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Author: Pierre-Julien VIAILLY, Centre Henri Becquerel, Rouen, France
:Version: """+VERSION+"""
:Last updated: """+LAST_UPDATE+"""
:Tags: Genomics, UMI, Illumina, Miseq, Variant Calling, Alignment


USAGE
===============================================

python3 umi-varcal.py call [required_arguments] [optional_arguments]


DEPENDENCIES
===============================================

-Python modules:   
                    msgpack
                    pyfasta
                    pysam
                    scipy.stats
                    statsmodels.stats.multitest


REQUIRED ARGUMENTS
===============================================

-f, --fasta=FILE (case sensitive)
	The reference genome in FASTA format with its index present
	in the same directory.
	It is recommended that you enter the FASTA file full path
	For example : /home/my_dir/my_subdir/hg19.fasta

-b, --bed=FILE (case sensitive)
	The panel design file in a BED format.
	It is recommended that you enter the BED file full path
	For example : /home/my_dir/my_subdir/design.bed

-i, --input=FILE (case sensitive)
	The input file SAM/BAM generated by the sequencer and from which
	the UMI tags have already been extracted from the sequences and
	and added at the end of the read ID, preceded by an '_'. This format
	is the only format accepted by the variant caller.
	It is recommended that you enter the input file full path
	For example : /home/my_dir/my_subdir/ind1.sam



OPTIONAL ARGUMENTS
===============================================

-p, --pileup=FILE (case sensitive)
	The PILEUP file for the SAM file previously dumped during
	a older analysis with the same BED and FASTA files. This 
	is not the pileup format generated by SAMtools. If specified,
	the tool will attempt te retrieve the counts of each position
	from the PILEUP file instead of rebuilding the pileup from 
	scratch, a process that is relatively fast and can save time.
	It is recommended that you enter the PILEUP file full path
	For example : /home/my_dir/my_subdir/ind1.pileup

-o, --output=DIR (case sensitive)
	The output directory in which your files will be generated.
	It is recommended that you enter the output dir full path
	For example : /home/my_dir/my_subdir/out_dir

-c, --cores=INT
	The number of cores to be used for the analysis.
	Using more cores won't necessarily reduce analysis time but
	will significantly increase RAM consumption. Using more cores
	is advantageous with smaller panels and larger SAM/BAM files.
	(Check documentation for more info).
	The value of this parameter defaults to 1.

--min_base_quality=INT
	The minimum value for which a base with a certain quality is
	considered as "good". Only bases with a quality >= min_base_quality
	will be considered.
	The value of this parameter defaults to 10 

--min_read_quality=INT
	The minimum value for which a read with a certain mean quality is
	considered as "good". Only reads with a quality >= min_read_quality
	will be considered.
	The value of this parameter defaults to 20 

--min_mapping_quality=INT
	The minimum value for which a read with a certain mapping 
	quality is considered as "good". Only bases with a mapping
	quality >= min_mapping_quality will be considered.
	The value of this parameter defaults to 20

--min_variant_depth=INT
	The minimum count for a variant with a certain raw count to be
	called. Only variants with a raw count >= min_variant_depth 
	will be called.
	The value of this parameter defaults to 5

--alpha=INT/FLOAT
	This is the type I error probability known as alpha level.
	Positions with a p-value/q-value >= alpha will be considered as
	noisy and variants in these positions won't be called.
	The value of this parameter defaults to 0.05 (5%)

--min_variant_umi=INT
	The minimum count for a variant with a certain unique UMIs 
	count to be called. Only variants with a unique UMI count >= 
	min_variant_umi will be called.
	The value of this parameter defaults to 5

--strand_bias_method=STR
	This parameter lets you specifiy the method you want to use
	to calculate variant strand bias. The value can only be 
	"default" or "torrent_suite". Choosing any of the methods 
	can have minimal effect on the final variants called and the SB
	value differs: the torrent suite method generates SB values [0.5;1]
	but the default method generates SB values [0;+Inf].
	Obviously, this parameter defaults to the value "default"

--max_strand_bias=INT/FLOAT
	The minimum strand bias value for a variant with a certain  
	negative and positive coverage to be called. Only variants 
	with a strand bias <= max_strand_bias will be called.
	The value of this parameter defaults to 1.0 for the default
	method and to 0.743 for the torrent suite method

--max_hp_length=INT
	The maximum homopolymer length for a variant at a certain  
	position in an homopolymer region to be called. Only variants 
	with a homopolymer length <= max_hp_length will be called.
	The value of this parameter defaults to 10

--gvcf=BOOL
	This is an experimental feature that is turned off by default
	and that, if set to True, will generate a gVCF file along
	the normal VCF file

--keep_pileup=BOOL
	This lets the user keep or delete the generated pileup file.
	By default, this parameter is set to True

--black_list=FILE
	A .txt file containing a list of variants respecting the HGVS
	format. The file must contain one variant per line only. The 
	variants in this file are black listed and will not appear in
	the final VCF file even if they pass all the filters

--white_list=FILE
	A .txt file containing a list of variants respecting the HGVS
	format. The file must contain one variant per line only. The 
	variants in this file are white listed and will be shown in
	the final VCF file even if they don't pass any of the filters.
	The same position can not be black and white listed at the same
	time: If the same location is found in the black and the white
	list, the position will be analyzed with the chosen filters

--min_phase_umi=INT
	The minimum number of commom UMI tags for phase variants to be
	called. Only variants with a common UMI count >= min_phase_umi 
	will be called. The value of this parameter defaults to 3

--min_phase_vaf_ratio=FLOAT
	The minimum allelic frequency ratio between phase variants to be
	called. Only variants with a VAF ratio >= min_phase_umi will be 
	called. The value of this parameter defaults to 0.8

--max_phase_distance=INT
	The maximum distance allowed between two phased variants. Only 
	phased variants with a distance <= max_phase_distance will be 
	called. The value of this parameter defaults to 150


EXAMPLE
===============================================

python3 umi-varcal.py call -i /home/.../test.sam -b /home/.../design.bed -f /home/.../hg19.fa -p /home/.../test.pileup -o /home/.../out -c 3 --min_base_quality 15 --min_read_quality 25 --min_mapping_quality 40 --min_variant_depth 8 --min_variant_umi 7 --strand_bias_method default --max_strand_bias 0.8 --alpha 0.01 --max_hp_length 5 --gvcf True --keep_pileup False --black_list black_list.txt --white_list white_list.txt --min_phase_umi 5 --min_phase_vaf_ratio 0.85 --max_phase_distance 135



OUTPUT
===============================================

1-  A VCF file (<INPUT_NAME>.vcf) containing all the variants that
    were successfully called.

2-  A gVCF file (<INPUT_NAME>.gvcf) containing all the variants that
    were successfully called + informations for the blocks in which
    no variant was detected.
	
3-  A VARIANTS file (<INPUT_NAME>.variants) containing all the variants
    that were successfully called. This file contains the same variants of
    the VCF file, in addition to detailed metrics about the variants.

4-  A PILEUP file (<INPUT_NAME>.pileup) corresponding to the entire pileup dumped.
    This file is produced in the output directory. This file can be used to skip 
    the pileup building and load it instead if the analysis was already done.\n\n"""

	messages.append(message)



	PrintProgramName()
	print(messages[tools.index(tool)])

	exit()